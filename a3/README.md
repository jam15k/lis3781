# LIS3781 - Advanced Database

## Jorge Morejon

### Assignment 3 Requirements:

*Two Parts:*

1. Tables and data: Using only SQL: Save as lis3781_a3_solutions.sql
2. SQL solutions (below)


#### README.md file should include the following items:

1. Screenshot of *your* SQL code;
2. Screenshot of *your* populated tables (w/in the Oracle environment);
3. Optional: SQL code for the required reports.
4. Bitbucket repo links: *Your* lis3781 Bitbucket repo link


#### Assignment Screenshots:


*Screenshot of SQL Code:*

![SQL](img/sql1.PNG)

![SQL](img/sql2.PNG)

*Screenshot of Populated tables on Oracle:*

![Oracle](img/oracle.PNG)

